# karma单元测试入门


   Karma是一个提升测试效率的工具，帮助开发者更好更快速地在多种环境下执行测试代码，拿到测试结果。在运行的时候，它会自动启动配置好的浏览器，同时也会启动一个node服务器，然后在启动好的浏览器中执行测试代码，并将测试代码执行结果返回node服务器，然后node服务器再打印出收到的执行结果。

    申明：下列命令行是在mac运行环境下。

## 1、初始化一个工程

* `mkdir karma-demo && cd karma-demo`
* `npm init -y`
   
## 2、安装Karma及其他依赖包 
__前提条件：__
本地已经安装了node.js，对npm安装依赖包已经掌握。mac安装时会出现没有权限，建议在命令行前增加sudo。

* ` $ npm install -g karma-cli `
* ` $ npm install karma --save-dev `
* ` $ npm install karma-jasmine --save-dev ` 
* ` $ npm install karma-phantomjs-launcher --save-dev `
* ` $ npm install phantomjs --save-dev`
* ` $ npm install jasmine-core --save-dev `
 
 
    karma-cli可以理解是生成karma测试配置文件脚手架，karma这里是本地安装，也可以全局安装，karma-phantomjs-launcher调用不同浏览器就需要不同的插件，这里使用的是无头浏览器，通俗点就是无界面，更多请看下面浏览器插件汇总。jasmine是单元测试框架。
  
  
  
```javascript
浏览器插件汇总
 * Chrome `karma-chrome-launcher`
 * ChromeCanary `karma-chrome-launcher`
 * PhantomJS `karma-phantomjs-launcher` 
 * Firefox `karma-firefox-launcher` 
 * Opera `karma-opera-launcher` 
 * IE `karma-ie-launcher` 
 * Safari `karma-safari-launcher` 
```
     

安装完成后使用 `karma start` 测试下是否安装成功。如果提示未找到karma你又确定安装成功了,请用 `./node_modules/karma/bin/karma start` 。我使用webstorm的命令窗口可以使用karma start，用系统命令窗口则需要使用后者。

成功如下图：
<img src="/img/karma-start.png" alt="karma start成功图" />


## 3、新建源文件
* `mkdir src`
* `touch utils.js`


utils.js内容：
```javascript
/**
 * 字符串翻转
 * @param name
 * @returns {string}
 */
function reverse(name) {
    return name.split("").reverse().join("");
}

/**
 * 判断是否是整数
 * @param num
 * @returns {boolean}
 */
function isInteger(num) {
    if (typeof num !== "number") return false;
    var pattern = /^[1-9]\d*$/g;
    return pattern.test(num);
}

/**
 * 判断value的数据类型
 * @param value
 */
function getTypeofVal(val) {
    if(typeof val == 'object'){
        return objType(val);
    }
    return typeof val;
}

/**
 * 获取对象的类型
 * @param arr
 * @returns {string}
 */
function objType(arr){
    return Object.prototype.toString.apply(arr);
}

```
## 4、新建单元测试文件
* `mkdir test`
* `touch index.spec.js`

index.spec.js内容：
```bash
/**
 * 测试整数  第一个单元测试expect(true).toEqual(isInteger(20));里面写了3种方式，都是可以的。
 */
describe("this is a integer test!", function () {
    it("Is integer", function () {
        expect(true).toEqual(isInteger(20));
        expect(isInteger(20)).toBe(true);
        expect(isInteger(20)).toEqual(true);
        expect(false).toEqual(isInteger("20"));
        expect(false).toEqual(isInteger(0));
        expect(false).toEqual(isInteger(0.1));
        expect(isInteger(0.1)).toEqual(false);
    })
});

/**
 * 测试倒序排列
 */
describe("this is a string reverse test!", function () {
    it("reverse String", function () {
        expect("DCBA").toEqual(reverse("ABCD"))
    })
});

/**
 * 测试数据类型
 */
describe("data type method test!", function () {
    it("base data type", function () {
        expect('number').toEqual(getTypeofVal(80));
        expect('string').toEqual(getTypeofVal("this is a string"));
        expect('number').toEqual(getTypeofVal(NaN));
        expect('boolean').toEqual(getTypeofVal(true));
    })

    it("object type",function(){
        expect('[object Null]').toEqual(objType(null));
    })
});


```

## 5、初始化Karma + Jasmine配置

1，初始化karma

* ` $ karma init`

初始化karma如下图：
<img src="/img/karma-init.png" alt="karma init初始化" />


2，配置文件karma.conf.js：

```bash
// Karma configuration
module.exports = function(config) {
  config.set({

    // 根路径，后面配置的基本所有相对路径都会根据这个路径来构造。
    basePath: '',


    // 使用到的框架
    // 目前支持的框架： https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // 告诉karma业务代码和测试代码在哪
    files: [
        './src/**/*.js',
        './test/**/*.spec.js'
    ],


    // 需要从 files 中排除掉的文件
    exclude: [
        'karma.conf.js'
    ],


    // 指定对应的JS文件 去执行代码的覆盖率，
    // 需要做预处理的文件，以及这些文件对应的预处理器。
    // 此处就可以将 coffee 、 ES6 等代码转换一下。
    preprocessors: {
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // 服务器端口号
    port: 9876,


    // 在输出内容（报告器和日志）中启用/禁用颜色
    colors: true,


    // 日志级别
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // 启用/禁用监视文件变化重新执行测试的功能
    autoWatch: true,


    // 要测试的目标环境 ,既然数组可以同时使用多个如：browsers: ['Chrome', 'Firefox', 'Safari'],
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // 命令窗口独立运行环境，不打开浏览器则时true.
    // 这个地方需要注意，如果选择了chrome等图形界面浏览器则是flase。
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}

```

## 5、代码测试覆盖率
*`npm install karma-coverage -D`

修改karma.conf.js配置文件
```bash
reporters: ['progress','coverage'],
preprocessors : {'./test/**/*.js': ['coverage']},
coverageReporter: {
    type : 'html',
    dir : 'coverage/'
}
```

## 6、执行并查看测试结果
* `karma start`

成功如下图：
<img src="/img/karma-success.png" alt="karma单元测试结果图" />

覆盖率运行coverage文件夹里面的index.html即可查看
如图：
<img src="/img/karma-coverage.png" alt="代码覆盖率结果图" />

最终项目结构
<img src="/img/karma-menu.png" alt="项目目录及文件" />

---
总结：karma是一个集成环境，按照上面步骤完成自己的单元测试案例。如果遇到问题或者文中有错误的地方，欢迎留言。